//
//  PushManager.swift
//  Diplona
//
//  Created by Veronica Rudiuk on 12.12.21.
//

import Foundation
import UserNotifications
import UIKit

class PushManager {
    
   class func requestAutoritization() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { success, error in
            if success {
                print("Пользователь разрешил использование использование пушей!")
                guard let trigger = createNotificationTrigger(duration: 100, components: nil, repeats: false)
                else { return }

                createNotification(trigger: trigger,
                                   content: createNotificationContent(title: "Hello 😀", subtitle: "Go to Atrium!🐮​💩​👍​🌺​🌹​"),
                                   identifier: UUID().uuidString)
                
                var components = DateComponents()
                components.day = 13
                components.hour = 19
                components.minute = 30
                
                guard let dateTriger = createNotificationTrigger(duration: nil, components: components, repeats: true) else  { return }
                createNotification(trigger: dateTriger,
                                   content: createNotificationContent(title: "Приветики ​🐮​💩​👍​🌺​🌹​", subtitle: "Красотка ​💅​👸​​💃​"),
                                   identifier: UUID().uuidString)
//                scheduleForDateComponent(components: components)
            }
            else if let error = error {
                print(error.localizedDescription)
                print("Пользователь запретил использование использование пушей")
            }
        }
    }
    

    class func createNotificationContent(title: String, subtitle: String) -> UNMutableNotificationContent {
        let content = UNMutableNotificationContent()
        content.title = title
        content.subtitle = subtitle
        content.sound = .default
        return content
    }
    
    class func createNotificationTrigger(duration: TimeInterval?, components: DateComponents?, repeats: Bool) ->
    UNNotificationTrigger? {
        if let duration = duration {
            let timeTrigger = UNTimeIntervalNotificationTrigger(timeInterval: duration, repeats: repeats)
            return timeTrigger
        }
        
        if let components = components {
            let componentsTrigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: repeats)
            return componentsTrigger
        }
        
        return nil
    }
    
    class func createNotification(trigger: UNNotificationTrigger, content: UNNotificationContent, identifier: String) {
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content,
                                            trigger: trigger)
        UNUserNotificationCenter.current().add(request)
    }
}
