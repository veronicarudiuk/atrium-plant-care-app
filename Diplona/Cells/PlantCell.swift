//
//  PlantCell.swift
//  Diplona
//
//  Created by Veronica Rudiuk on 12.12.21.
//

import UIKit

class PlantCell: UITableViewCell {
    @IBOutlet weak var plantName: UILabel!
    @IBOutlet weak var wasPostponedLabel: UILabel!
    @IBOutlet weak var wasPostponedView: UIView!
    @IBOutlet weak var wateringLabel: UILabel!
    @IBOutlet weak var wateringView: UIView!
    @IBOutlet weak var transplantingLabel: UILabel!
    @IBOutlet weak var transplantingView: UIView!
    @IBOutlet weak var fertilizerLabel: UILabel!
    @IBOutlet weak var fertilizerView: UIView!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var plantImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        wasPostponedLabel.addCharacterSpacing(kernValue: -1.0)
        wateringLabel.addCharacterSpacing(kernValue: -1.0)
        transplantingLabel.addCharacterSpacing(kernValue: -1.0)
        fertilizerLabel.addCharacterSpacing(kernValue: -1.0)
        wasPostponedView.layer.cornerRadius = 9
        wateringView.layer.cornerRadius = 10
        transplantingView.layer.cornerRadius = 10
        fertilizerView.layer.cornerRadius = 10
        plantImage.layer.cornerRadius = 20
        
        mainView.layer.cornerRadius = 30
        mainView.layer.shadowOpacity = 1
        mainView.layer.shadowRadius = 10

        mainView.layer.shadowOffset = CGSize(width: 0, height: 5)
        mainView.layer.shadowColor = UIColor(named: "PlantCellShadow")?.cgColor ?? UIColor.clear.cgColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpCell(plant: Plant) {
        plantName.text = "\(plant.name)"

        plantImage.image =   UIImage(data: (plant.photoData))
    
    }
}

