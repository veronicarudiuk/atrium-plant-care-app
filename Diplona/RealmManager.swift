//
//  RealmManager.swift
//  Diplona
//
//  Created by Veronica Rudiuk on 3.01.22.
//

import Foundation

import RealmSwift

class RealmManager {
    private let realm = try! Realm() // Доступ к хранилищу
    static let shared = RealmManager() // Переменная со свойствами объекта PlantList
    private init() {}
    
    func getPlant() -> [Plant] {
        return Array(realm.objects(Plant.self))
    }
    
    func writePlant(plant: Plant) {
        try! realm.write {
            realm.add(plant)
        }
    }
    
    func deletePlant(plant: Plant) {
        try! realm.write {
            realm.delete(plant)
        }
    }
}
