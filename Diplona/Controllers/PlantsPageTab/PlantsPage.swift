//
//  PlantsPage.swift
//  Diplona
//
//  Created by Veronica Rudiuk on 29.11.21.
//

import UIKit
import SwiftUI

class PlantsPage: UIViewController {
    
    
    @IBOutlet weak var backgroundGradientView: UIView!
    @IBOutlet weak var addButtonView: UIView!
    @IBOutlet weak var addButton: UIButton!
    
    @IBOutlet weak var tableViewCalendar: UITableView!
    @IBOutlet weak var tableViewPlants: UITableView!
    
    @IBOutlet weak var selectCareOrPlantMainView: UIView!
    @IBOutlet weak var selectCareOrPlantSelectedView: UIView!
    @IBOutlet weak var careButtonOutlet: UIButton!
    @IBOutlet weak var yourPlantButtonOutlet: UIButton!
    
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var emptyLabelOne: UILabel!
    @IBOutlet weak var emptyLabelTwo: UILabel!
  
    @IBOutlet weak var bottomViewOver: UIView!
    @IBOutlet weak var bottomViewUnder: UIView!
    
    var plants: [Plant] = [] {
        didSet {
            tableViewCalendar.reloadData()
            tableViewPlants.reloadData()
            
            if plants.count == 0 {
                emptyLabelOne.isHidden = false
                emptyLabelTwo.isHidden = false
//                youHaveNPlantsLabel.alpha = 0
                
            } else if plants.count == 1 {
                emptyLabelOne.isHidden = true
                emptyLabelTwo.isHidden = true
//                youHaveNPlantsLabel.text = "You one plant"
            }
            else if plants.count > 1 {
                emptyLabelOne.isHidden = true
                emptyLabelTwo.isHidden = true
//                youHaveNPlantsLabel.text = "You have \(plants.count) plants"
            }
        }
    }
    
//    let calendar = Calendar.current
//    let daterr = calendar.date(byAdding: .day, value: daysStrong, to: lastWateringStrong)
//    print(formatter3.string(from: (daterr)!))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        tableViewPlants.alpha = 0

        
        
        //        настройка градиента фона
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [UIColor(named: "BackgroundGradient1")?.cgColor ?? UIColor.clear.cgColor,
                                UIColor(named: "BackgroundGradient2")?.cgColor ?? UIColor.clear.cgColor]
        gradientLayer.shouldRasterize = true
        backgroundGradientView.layer.addSublayer(gradientLayer)
        
        //        настройка кнопки на таб баре
        addButtonView.layer.cornerRadius = 30
        self.view.bringSubviewToFront(addButton)
        
        //        убираю разделитель в таблице
        tableViewCalendar.separatorStyle = .none
        tableViewPlants.separatorStyle = .none
        
        //        настройка картомного segmented control
        selectCareOrPlantMainView.layer.cornerRadius = 25
        selectCareOrPlantSelectedView.layer.cornerRadius = 19
        selectCareOrPlantSelectedView.layer.shadowOpacity = 1
        selectCareOrPlantSelectedView.layer.shadowRadius = 5
        
        selectCareOrPlantSelectedView.layer.shadowOffset = CGSize(width: 0, height: 5)
        selectCareOrPlantSelectedView.layer.shadowColor = UIColor(named: "SegmentedControlShadow")?.cgColor ?? UIColor.clear.cgColor
        
        
        careButtonOutlet.setAttributedTitle(NSAttributedString(string: "Care", attributes: [.font: UIFont(name: "SF Compact Rounded Semibold", size: 14)!]), for: .normal)
        careButtonOutlet.setAttributedTitle(NSAttributedString(string: "Care", attributes: [.font: UIFont(name: "SF Compact Rounded Semibold", size: 14)!]), for: .selected)
//        careButtonOutlet.titleLabel?.font = UIFont(name: "SF Compact Rounded Semibold", size: 14)
        careButtonOutlet.titleLabel?.textColor = UIColor(named: "Text")!
        yourPlantButtonOutlet.setAttributedTitle(NSAttributedString(string: "Your plants", attributes: [.font: UIFont(name: "SF Compact Rounded Semibold", size: 14)!]), for: .normal)
        yourPlantButtonOutlet.setAttributedTitle(NSAttributedString(string: "Your plants", attributes: [.font: UIFont(name: "SF Compact Rounded Semibold", size: 14)!]), for: .selected)
        yourPlantButtonOutlet.titleLabel?.textColor = UIColor(named: "InactiveText2")!

        bottomViewOver.layer.shadowColor = UIColor(named: "TabBarShadowColor")?.cgColor ?? UIColor.clear.cgColor
        bottomViewOver.layer.shadowOpacity = 1
        bottomViewOver.layer.shadowRadius = 50
        bottomViewOver.layer.cornerRadius = 40
        bottomViewUnder.layer.cornerRadius = 20
        
    }
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        if let indeedTabBarController = self.tabBarController {
//            indeedTabBarController.view.addSubview(addButton)
//        }
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        plants.removeAll()
        plants = RealmManager.shared.getPlant()
        
    }
    
    //        настройка таблицы
    func configureTableView() {
        tableViewCalendar.dataSource = self
        tableViewCalendar.delegate = self
        tableViewCalendar.tableFooterView = UIView()
        let nib = UINib(nibName: String(describing: PlantCell.self), bundle: nil)
        tableViewCalendar.register(nib, forCellReuseIdentifier: String(describing: PlantCell.self))
        
        tableViewPlants.dataSource = self
        tableViewPlants.delegate = self
        tableViewPlants.tableFooterView = UIView()
        tableViewPlants.register(nib, forCellReuseIdentifier: String(describing: PlantCell.self))
        
    }
    
    
    @IBAction func addPlantButton(_ sender: Any) {
        let addPlanController = AddPlanController(nibName: String(describing: AddPlanController.self), bundle: nil)
        addPlanController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(addPlanController, animated: true)
    }
    
    @IBAction func careButton(_ sender: Any) {
        leadingConstraint.priority = UILayoutPriority(rawValue: 800)
        trailingConstraint.priority = UILayoutPriority(rawValue: 800)
        UIView.animate(withDuration: 0.2) {
            self.careButtonOutlet.titleLabel?.textColor = UIColor(named: "Text")!
            self.careButtonOutlet.titleLabel?.font = UIFont(name: "SF Compact Rounded Semibold", size: 14)
            self.yourPlantButtonOutlet.titleLabel?.textColor = UIColor(named: "InactiveText2")!
            self.yourPlantButtonOutlet.titleLabel?.font = UIFont(name: "SF Compact Rounded Semibold", size: 14)
            self.tableViewPlants.alpha = 0
            self.tableViewCalendar.alpha = 1
//            self.youHaveNPlantsLabel.alpha = 0
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func yourPlantButton(_ sender: Any) {
        leadingConstraint.priority = UILayoutPriority(rawValue: 600)
        trailingConstraint.priority = UILayoutPriority(rawValue: 600)
        UIView.animate(withDuration: 0.2) {
            self.careButtonOutlet.titleLabel?.textColor = UIColor(named: "InactiveText2")!
            self.careButtonOutlet.titleLabel?.font = UIFont(name: "SF Compact Rounded Semibold", size: 14)
            self.yourPlantButtonOutlet.titleLabel?.font = UIFont(name: "SF Compact Rounded Semibold", size: 14)
            self.yourPlantButtonOutlet.titleLabel?.textColor = UIColor(named: "Text")!
            self.tableViewPlants.alpha = 1
            self.tableViewCalendar.alpha = 0
//            self.youHaveNPlantsLabel.alpha = 1
            self.view.layoutIfNeeded()
        }
    }
}


extension PlantsPage: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return plants.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tableViewCalendar {
            return 10
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PlantCell.self), for: indexPath)
        guard let plantCell = cell as? PlantCell else { return cell }
        plantCell.setUpCell(plant: plants[indexPath.row])
        return plantCell
    }
    
}

extension PlantsPage: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let edidingRow = plants[indexPath.row]
            self.plants.remove(at: indexPath.row)
            RealmManager.shared.deletePlant(plant: edidingRow)
            self.tableViewCalendar.reloadSections(IndexSet(integer: indexPath.section), with: .automatic)
        }
    }
    
    

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if plants.count > 0 {
        var header = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
            if tableView == tableViewCalendar {
                header = UIView(frame: CGRect(x: 0, y: 0, width: 374, height: 26))
                let dayLabel = UILabel(frame: CGRect(x: 20, y: -8, width: 374, height: 26))
                header.addSubview(dayLabel)
                dayLabel.textColor = UIColor(named: "Text")!
                dayLabel.text = "Wensday, Jun. 19"
                dayLabel.font = UIFont(name: "SF Compact Text Semibold", size: 22.0)
            } else if tableView == tableViewPlants {
                header = UIView(frame: CGRect(x: 0, y: 0, width: 374, height: 26))
                let dayLabel = UILabel(frame: CGRect(x: 20, y: -8, width: 374, height: 26))
                header.addSubview(dayLabel)
                dayLabel.textColor = UIColor(named: "Text")!
                
                if plants.count == 1 {
                    dayLabel.text = "You have one plant"
                }
                else if plants.count > 1 {
                    dayLabel.text = "You have \(plants.count) plants"
                }
                dayLabel.font = UIFont(name: "SF Compact Text Semibold", size: 22.0)
            }
            
            return header
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if tableView == tableViewCalendar {
            return 4
        } else {
            return 0
        }
    }
}





//        MARK: настройка простого SegmentedControl

//        let colorNormal : UIColor = UIColor(named: "InactiveText2")!
//        let colorSelected : UIColor = UIColor(named: "Text")!
//        //        Fatal error: Unexpectedly found nil while unwrapping an Optional value
//        let titleFont : UIFont = UIFont(name: "SF Compact Rounded Semibold", size: 12) ?? UIFont.systemFont(ofSize: 20)
//        let attributesNormal = [NSAttributedString.Key.foregroundColor : colorNormal,
//                                NSAttributedString.Key.font : titleFont]
//        let attributesSelected = [NSAttributedString.Key.foregroundColor : colorSelected,
//                                  NSAttributedString.Key.font : titleFont]
//        UISegmentedControl.appearance().setTitleTextAttributes(attributesNormal, for: .normal)
//        UISegmentedControl.appearance().setTitleTextAttributes(attributesSelected, for: .selected)
