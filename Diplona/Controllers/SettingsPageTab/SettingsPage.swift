//
//  SettingsPage.swift
//  Diplona
//
//  Created by Veronica Rudiuk on 18.01.22.
//

import UIKit

class SettingsPage: UIViewController {
 
    @IBOutlet weak var backgroundGradientView: UIView!
    @IBOutlet weak var addButtonView: UIView!
    @IBOutlet weak var addButton: UIButton!
    
    @IBOutlet weak var subscribeView: UIView!
    @IBOutlet weak var subscribeImage: UIImageView!
    @IBOutlet weak var subscribeMainTitle: UILabel!
    @IBOutlet weak var subscribeTryProLabel: UILabel!
    @IBOutlet weak var subscribeImageView: UIView!
    
    @IBOutlet weak var notificationsView: UIView!
    @IBOutlet weak var contactSupportView: UIView!
    @IBOutlet weak var termOfUseView: UIView!
    @IBOutlet weak var privacyPolicyView: UIView!
    @IBOutlet weak var shareWithFriends: UIView!
    
    @IBOutlet weak var bottomViewOver: UIView!
    @IBOutlet weak var bottomViewUnder: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        //        настройка градиента фона
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [UIColor(named: "BackgroundGradient1")?.cgColor ?? UIColor.clear.cgColor,
                                UIColor(named: "BackgroundGradient2")?.cgColor ?? UIColor.clear.cgColor]
        gradientLayer.shouldRasterize = true
        backgroundGradientView.layer.addSublayer(gradientLayer)
        
        //        настройка кнопки на таб баре
        addButtonView.layer.cornerRadius = 30
        self.view.bringSubviewToFront(addButton)
        
        //        настройка блока подписки
        subscribeImageView.layer.cornerRadius = 20
        subscribeImageView.layer.shadowOpacity = 1
        subscribeImageView.layer.shadowRadius = 10
        subscribeImageView.layer.shadowOffset = CGSize(width: 0, height: 4)
        subscribeImageView.layer.shadowColor = UIColor(named: "SubscribeImageShadow")?.cgColor ?? UIColor.clear.cgColor
        
        subscribeView.layer.cornerRadius = 30
        subscribeView.layer.shadowOpacity = 1
        subscribeView.layer.shadowRadius = 40
        subscribeView.layer.shadowOffset = CGSize(width: 0, height: 5)
        subscribeView.layer.shadowColor = UIColor(named: "PlantCellShadow")?.cgColor ?? UIColor.clear.cgColor
        
        viewUiSettings(view: notificationsView)
        viewUiSettings(view: contactSupportView)
        viewUiSettings(view: termOfUseView)
        viewUiSettings(view: privacyPolicyView)
        viewUiSettings(view: shareWithFriends)
        
        subscribeTryProLabel.addCharacterSpacing(kernValue: -0.57)
        
        
        bottomViewOver.layer.shadowColor = UIColor(named: "TabBarShadowColor")?.cgColor ?? UIColor.clear.cgColor
        bottomViewOver.layer.shadowOpacity = 1
        bottomViewOver.layer.shadowRadius = 50
        bottomViewOver.layer.cornerRadius = 40
        bottomViewUnder.layer.cornerRadius = 20
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        if let indeedTabBarController = self.tabBarController {
//            indeedTabBarController.view.addSubview(addButton)
//        }
//    }


    func viewUiSettings(view: UIView) {
        view.layer.cornerRadius = 20
        view.layer.shadowOpacity = 1
        view.layer.shadowRadius = 40
        view.layer.shadowOffset = CGSize(width: 0, height: 5)
        view.layer.shadowColor = UIColor(named: "PlantCellShadow")?.cgColor ?? UIColor.clear.cgColor
    }

    @IBAction func addPlantButton(_ sender: Any) {
        let addPlanController = AddPlanController(nibName: String(describing: AddPlanController.self), bundle: nil)
        addPlanController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(addPlanController, animated: true)
    }
    
}
