//
//  AddPlanController.swift
//  Diplona
//
//  Created by Veronica Rudiuk on 3.01.22.
//

import UIKit
import RealmSwift

class PeriodsOfFrequency {
    var period: String
    var numbers: [Int]
    
    init(period: String, numbers: [Int]) {
        self.period = period
        self.numbers = numbers
    }
}


class AddPlanController: UIViewController,
                         UITextFieldDelegate,
                         UIImagePickerControllerDelegate,
                         UINavigationControllerDelegate{
    
    @IBOutlet weak var backgroundGradientView: UIView!
    @IBOutlet weak var plantsNameInput: UITextField!
    
    
    @IBOutlet weak var saveButtonView: UIView!
    @IBOutlet weak var saveButtonOutlet: UIButton!
    @IBOutlet weak var imagePicked: UIImageView!
    
    @IBOutlet weak var wateringView: UIView!
    @IBOutlet weak var wateringLabel: UILabel!
    
    
    @IBOutlet weak var fertilizerView: UIView!
    @IBOutlet weak var fertilizerLabel: UILabel!
    
    
    @IBOutlet weak var transplantingView: UIView!
    @IBOutlet weak var transplantingLabel: UILabel!
    
    @IBOutlet weak var isolationPeriodView: UIView!
    @IBOutlet weak var isolationPeriodSwitch: UISwitch!
    
    
    
    @IBOutlet weak var settingBlackBackground: UIView!
    @IBOutlet weak var settingMainView: UIView!
    
    @IBOutlet weak var settingGrayLine: UIView!
    @IBOutlet weak var settingDoneButton: UIButton!
    @IBOutlet weak var settingMainLabel: UILabel!
    @IBOutlet weak var settingDeleteButtonLabel: UIButton!
    
    @IBOutlet weak var frequencyPicker: UIPickerView!
    @IBOutlet weak var frequencyView: UIView!
    @IBOutlet weak var frequencyPeriodLabel: UILabel!
    @IBOutlet weak var frequencyLeftLabel: UILabel!
    
    
    
    @IBOutlet weak var lastDayPicker: UIDatePicker!
    @IBOutlet weak var lastDayView: UIView!
    @IBOutlet weak var lastDayPeriodLabel: UILabel!
    @IBOutlet weak var lastLeftLabel: UILabel!
    
    
    var pickedImage: UIImage?
    var lastWateringWeak: Date = Date()
    var lastWateringStrong: Date = Date()
    var periodOfFrequency = [PeriodsOfFrequency]()
    
    var periodMain = ""
    var numberMain = 0
    var daysWeak = 0
    var daysStrong = 0
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.plantsNameInput.delegate = self
        backgroundGradient()
        saveButtonViewGradient()
        backgroundGradientSettingsView()
        
        
        lastDayPicker?.addTarget(self, action: #selector(AddPlanController.dateChangedLabel(datePicker:)), for: .valueChanged)
        
        settingBlackBackground.alpha = 0
        settingMainView.alpha = 0
        
        imagePicked.layer.cornerRadius = 30
        
        //        настройка кнопки добавления
        saveButtonOutlet.layer.cornerRadius = 25
        saveButtonOutlet.titleLabel?.font = UIFont(name: "SF Compact Rounded Bold", size: 18)
        
        //        настройка тест филда
        let spacerView = UIView(frame:CGRect(x:0, y:0, width:20, height:20))
        plantsNameInput.leftViewMode = UITextField.ViewMode.always
        plantsNameInput.leftView = spacerView
        plantsNameInput.borderStyle = .none
        plantsNameInput.layer.cornerRadius = 20
        
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor(named: "InactiveTextTextField")!,
                          .font: UIFont(name: "SF Compact Rounded Regular", size: 18)]
        
        plantsNameInput.attributedPlaceholder = NSAttributedString(string: "Enter a name", attributes: attributes as [NSAttributedString.Key : Any])
        
        //        настройки вида кнопок перехода
        wateringView.layer.cornerRadius = 20
        wateringView.layer.borderWidth = 2
        wateringView.layer.borderColor = UIColor(named: "WateringButtonBorder")?.cgColor
        
        fertilizerView.layer.cornerRadius = 20
        fertilizerView.layer.borderWidth = 2
        fertilizerView.layer.borderColor = UIColor(named: "WateringButtonBorder")?.cgColor
        
        transplantingView.layer.cornerRadius = 20
        transplantingView.layer.borderWidth = 2
        transplantingView.layer.borderColor = UIColor(named: "WateringButtonBorder")?.cgColor
        
        //        настройка вида блока с изоляцией
        isolationPeriodView.layer.cornerRadius = 20
        isolationPeriodView.layer.borderWidth = 2
        isolationPeriodView.layer.borderColor = UIColor(named: "WateringButtonBorder")?.cgColor
        
        
        
        //        настройка экрана с настройками
        settingGrayLine.layer.cornerRadius = 2
        settingMainView.roundCorners(corners: [.topLeft, .topRight], radius: 40)
        
        
        settingDeleteButtonLabel.layer.cornerRadius = 16
        settingDeleteButtonLabel.setAttributedTitle(NSAttributedString(string: "Delete reminder", attributes: [.font: UIFont(name: "SF Compact Rounded Medium", size: 14) ?? UIFont.systemFont(ofSize: 14)]), for: .normal)
        settingDeleteButtonLabel.setAttributedTitle(NSAttributedString(string: "Delete reminder", attributes: [.font: UIFont(name: "SF Compact Rounded Medium", size: 14) ?? UIFont.systemFont(ofSize: 14)]), for: .selected)
     
        
        settingDoneButton.setAttributedTitle(NSAttributedString(string: "Done", attributes: [.font: UIFont(name: "SF Compact Rounded Semibold", size: 14)!]), for: .normal)
        settingDoneButton.setAttributedTitle(NSAttributedString(string: "Done", attributes: [.font: UIFont(name: "SF Compact Rounded Semibold", size: 14)!]), for: .selected)
        
                                                                                        
        settingDoneButton.layer.cornerRadius = 11

        frequencyView.layer.cornerRadius = 20
        lastDayView.layer.cornerRadius = 20
        
        //                настройка пикеров
        frequencyPicker.dataSource = self
        frequencyPicker.delegate = self
        
        periodOfFrequency.append(PeriodsOfFrequency(period: "days", numbers: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))
        periodOfFrequency.append(PeriodsOfFrequency(period: "weeks", numbers: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))
        periodOfFrequency.append(PeriodsOfFrequency(period: "months", numbers: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))
        periodOfFrequency.append(PeriodsOfFrequency(period: "years", numbers: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))
        
        
//        lastDayPicker.setValue(UIColor(named: "GreenButton"), forKey: "textColor")

        
        
    }
    
    
    
    //        настройка градиента фона
    func backgroundGradient() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = backgroundGradientView.bounds
        gradientLayer.colors = [UIColor(named: "BackgroundGradient1")?.cgColor ?? UIColor.clear.cgColor,
                                UIColor(named: "BackgroundGradient2")?.cgColor ?? UIColor.clear.cgColor]
        gradientLayer.shouldRasterize = true
        backgroundGradientView.layer.insertSublayer(gradientLayer, at:0)
    }
    
    //        настройка градиента вьюхи кнопки добавления
    func saveButtonViewGradient() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = saveButtonView.bounds
        gradientLayer.colors = [UIColor(named: "AddPlantButtonViewGradientNoAlpha")?.cgColor ?? UIColor.clear.cgColor,
                                UIColor(named: "AddPlantButtonViewGradient")?.cgColor ?? UIColor.clear.cgColor]
        gradientLayer.shouldRasterize = true
        saveButtonView.layer.insertSublayer(gradientLayer, at:0)
    }
    
    //        настройка градиента фона экрана с настройками
    func backgroundGradientSettingsView() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = settingMainView.bounds
        gradientLayer.colors = [UIColor(named: "BackgroundGradient1")?.cgColor ?? UIColor.clear.cgColor,
                                UIColor(named: "BackgroundGradient2")?.cgColor ?? UIColor.clear.cgColor]
        gradientLayer.shouldRasterize = true
        settingMainView.layer.insertSublayer(gradientLayer, at:0)
    }
    
    
    //    функции для скрывания клавиатуры
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return plantsNameInput.resignFirstResponder()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //    функции для загрузки фотки
    @IBAction func openPhotoLibraryButton(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            //            imagePicker.accessibilityFrame =
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            imagePicker.preferredContentSize = CGSize(width: 250, height: 383)
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:-- ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.editedImage] as? UIImage {
            imagePicked.image = pickedImage
            imagePicked.contentMode = .scaleToFill // сжимает, а не обрезает .scaleAspectFit
            self.pickedImage = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    @objc func dateChangedLabel(datePicker: UIDatePicker){
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, MMM. d"
        lastDayPeriodLabel.text = formatter.string(from: lastDayPicker.date)
        lastWateringWeak = lastDayPicker.date
    }
    
    
    @IBAction func arrowBackAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func wateringButton(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5) { [self] in
            self.settingMainView.alpha = 1
            self.settingBlackBackground.alpha = 1
            self.settingMainLabel.text = "Watering"
            self.frequencyLeftLabel.text = "Watering frequency"
            self.lastLeftLabel.text = "Last watering"
        }
    }
    
    @IBAction func fertilizerButton(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5) { [self] in
            self.settingMainView.alpha = 1
            self.settingBlackBackground.alpha = 1
            self.settingMainLabel.text = "Fertilizer"
            self.frequencyLeftLabel.text = "Fertilizer frequency"
            self.lastLeftLabel.text = "Last fertilizer"
        }
    }
    
    @IBAction func transplantingButton(_ sender: Any) {
        UIView.animate(withDuration: 0.5) { [self] in
            self.settingMainView.alpha = 1
            self.settingBlackBackground.alpha = 1
            self.settingMainLabel.text = "Transplanting"
            self.frequencyLeftLabel.text = "Transplanting frequency"
            self.lastLeftLabel.text = "Last transplanting"
        }
    }
    
    
    @IBAction func hideSettingsView(_ sender: Any) {
        UIView.animate(withDuration: 0.5) { [self] in
            self.settingMainView.alpha = 0
            self.settingBlackBackground.alpha = 0
        }
    }
    
    @IBAction func settingsDoneButton(_ sender: Any) {
        if settingMainLabel.text == "Watering" {
            wateringLabel.text = frequencyPeriodLabel.text
            lastWateringStrong = lastWateringWeak
            daysStrong = daysWeak
        }
        if settingMainLabel.text == "Fertilizer" {
            fertilizerLabel.text = frequencyPeriodLabel.text
        }
        if settingMainLabel.text == "Transplanting" {
            transplantingLabel.text = frequencyPeriodLabel.text
        }
        UIView.animate(withDuration: 0.5) { [self] in
            self.settingMainView.alpha = 0
            self.settingBlackBackground.alpha = 0
        }
        
        
    }
    
    @IBAction func deleteReminderAction(_ sender: Any) {
        if settingMainLabel.text == "Watering" {
            wateringLabel.text = "None"
            lastWateringStrong = Date()
            daysStrong = 0
        }
        if settingMainLabel.text == "Fertilizer" {
            fertilizerLabel.text = "None"
        }
        if settingMainLabel.text == "Transplanting" {
            transplantingLabel.text = "None"
        }
        UIView.animate(withDuration: 0.5) { [self] in
            self.settingMainView.alpha = 0
            self.settingBlackBackground.alpha = 0
        }
        
    }
    
    
    @IBAction func saveButtonAction(_ sender: Any) {
        guard let name = plantsNameInput.text, !name.isEmpty else { return }
        
        let lastWatering = lastWateringStrong
        let wateringfrequency = daysStrong
        
        let formatter3 = DateFormatter()
        formatter3.dateFormat = "EEEE, MMM. d"
        
        if let pickedImage = pickedImage {
            let plant = Plant(name: name, photoData: pickedImage.jpegData(compressionQuality: 1), wateringfrequency: wateringfrequency, lastWatering: lastWatering)
            RealmManager.shared.writePlant(plant: plant)
//            print(formatter3.string(from: plant.dayNeedToWatering!))
        } else {
            let plant = Plant(name: name, wateringfrequency: wateringfrequency, lastWatering: lastWatering)
            RealmManager.shared.writePlant(plant: plant)
//            print(formatter3.string(from: plant.dayNeedToWatering ?? Date()))
        }
        
        
        print(formatter3.string(from: lastWatering))
//        print(Realm.object(plant.tim))
        
        
        navigationController?.popViewController(animated: true)
    }
    
}


extension AddPlanController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return periodOfFrequency.count
        }
        else {
            let selectedPeriod = pickerView.selectedRow(inComponent: 0)
            return periodOfFrequency[selectedPeriod].numbers.count
        }
    }
}

extension AddPlanController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return periodOfFrequency[row].period
        }
        else {
            let selectedPeriod = pickerView.selectedRow(inComponent: 0)
            return String(periodOfFrequency[selectedPeriod].numbers[row])
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerView.reloadComponent(0)
        pickerView.reloadComponent(1)
        let selectedPeriod = pickerView.selectedRow(inComponent: 0)
        let selectedNumber = pickerView.selectedRow(inComponent: 1)

        let period = periodOfFrequency[selectedPeriod].period
        periodMain = period
        let number = periodOfFrequency[selectedPeriod].numbers[selectedNumber]
        numberMain = number
        frequencyPeriodLabel.text = "Once every \(number) \(period)"
        
        if periodMain == "days" {
            daysWeak = 1 * numberMain
        } else if periodMain == "weeks" {
            daysWeak = 7 * numberMain
        } else if periodMain == "months" {
            daysWeak = 30 * numberMain
        } else if periodMain == "years" {
            daysWeak = 365 * numberMain
        } else {
            daysWeak = 0
        }
    }
}


