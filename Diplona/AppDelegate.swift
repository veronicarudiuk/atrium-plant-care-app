//
//  AppDelegate.swift
//  Diplona
//
//  Created by Veronica Rudiuk on 29.11.21.
//

import UIKit
import IQKeyboardManagerSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

        var window: UIWindow?

    internal func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

          IQKeyboardManager.shared.enable = true
        UNUserNotificationCenter.current().delegate = self
          return true
        }
    

   
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        return completionHandler([.banner, .badge, .sound])
    }


}

