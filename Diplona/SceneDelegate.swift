//
//  SceneDelegate.swift
//  Diplona
//
//  Created by Veronica Rudiuk on 29.11.21.
//

import UIKit
import SwiftUI

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
//        configureNavbar()
        guard let windowScene = (scene as? UIWindowScene) else { return }
//        configureTabBar()
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        window?.rootViewController = createTabBar()
        window?.makeKeyAndVisible()
        PushManager.requestAutoritization()
        
    }

    func createTabBar() -> UITabBarController {
        let tabBarController = UITabBarController()
        
//        настройка шрифта, размера и цвета
    
        let colorNormal : UIColor = UIColor(named: "InactiveText")!
        let colorSelected : UIColor = UIColor(named: "Text")!
        //        Fatal error: Unexpectedly found nil while unwrapping an Optional value
        let titleFont : UIFont = UIFont(name: "SF Compact Rounded Semibold", size: 12) ?? UIFont.systemFont(ofSize: 20)
        let attributesNormal = [
            NSAttributedString.Key.foregroundColor : colorNormal,
            NSAttributedString.Key.font : titleFont
        ]
        let attributesSelected = [
            NSAttributedString.Key.foregroundColor : colorSelected,
            NSAttributedString.Key.font : titleFont
        ]
        UITabBarItem.appearance().setTitleTextAttributes(attributesNormal, for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes(attributesSelected, for: .selected)
        
        tabBarController.tabBar.tintColor = UIColor(named: "Text")!
       
        
    

        
        
        let firstItem = UITabBarItem(title: "Plants", image: UIImage(named: "PlantsIconInactive"), tag: 0)
        firstItem.selectedImage = UIImage(named: "PlantsIconActive")
        let first = PlantsPage(nibName: String(describing: PlantsPage.self), bundle: nil)
        first.tabBarItem = firstItem
        
        let secondItem = UITabBarItem(title: "Setting", image: UIImage(named: "SettingIconInactive"), tag: 1)
        secondItem.selectedImage = UIImage(named: "SettingIconActive")
        let second = SettingsPage(nibName: String(describing: SettingsPage.self), bundle: nil)
        second.tabBarItem = secondItem
        
        
//        tabBarController.tabBar.unselectedItemTintColor
        
        let controllers: [UINavigationController] = [UINavigationController(rootViewController: first),
                                                     UINavigationController(rootViewController: second)]
        
//        указание что наш массив будет теми контроллерами, которые храняться в таббаре
        tabBarController.viewControllers = controllers
        
//        установка внешнего вида таббара
        tabBarController.tabBar.backgroundColor = .clear
//        tabBarController.tabBar.layer.cornerRadius = 20
//        tabBarController.tabBar.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
//        tabBarController.tabBar.layer.shadowColor = UIColor(named: "TabBarShadowColor")?.cgColor ?? UIColor.clear.cgColor
////        не работает тень
//        tabBarController.tabBar.layer.shadowOpacity = 1
//        tabBarController.tabBar.layer.shadowRadius = 50
        
        return tabBarController
    }
}
    
    
    
//    func configureTabBar() {
//            if #available(iOS 13.0, *) {
//                let tabBarAppearance: UITabBarAppearance = UITabBarAppearance()
//                let tabBarItemAppearance = UITabBarItemAppearance()
//
//                if let font = UIFont(name: "SFCompactRoundedSemibold", size: 40), let selectedColor = UIColor(named: "Text"), let unselectedColor = UIColor(named: "InactiveText") {
//                    tabBarItemAppearance.normal.titleTextAttributes = [NSAttributedString.Key.foregroundColor: unselectedColor]
//                    tabBarItemAppearance.selected.titleTextAttributes = [NSAttributedString.Key.foregroundColor: selectedColor]
//                    tabBarItemAppearance.normal.titleTextAttributes = [NSAttributedString.Key.font: font]
//                    tabBarItemAppearance.selected.titleTextAttributes = [NSAttributedString.Key.font: font]
//                }
//
//                tabBarAppearance.stackedLayoutAppearance = tabBarItemAppearance
//
//                UITabBar.appearance().standardAppearance = tabBarAppearance
//
////                    if #available(iOS 15.0, *) {
////                        UITabBar.appearance().scrollEdgeAppearance = tabBarAppearance
////                    }
//            }
//        }
    
//    func configureNavbar() {
//        if #available(iOS 15, *) {
//            let appearance = UINavigationBarAppearance()
//            appearance.configureWithOpaqueBackground()
//            UINavigationBar.appearance().standardAppearance = appearance
//            UINavigationBar.appearance().scrollEdgeAppearance = appearance
//        }
//    }

