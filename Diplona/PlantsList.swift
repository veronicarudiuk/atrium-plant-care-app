//
//  Plants.swift
//  Diplona
//
//  Created by Veronica Rudiuk on 2.01.22.
//

import Foundation
import RealmSwift

//Object - класс для создания моделей
class Plant: Object {
//    определяем свойства моделей
    @objc dynamic var name = ""
    @objc dynamic var completed = false
    @objc dynamic var photoData = Data()
    
    @objc dynamic var wateringfrequency = 0
    @objc dynamic var lastWatering: Date = Date()
    @objc dynamic var dayNeedToWatering: Date?
    

//    @objc dynamic var wateringfrequency =
//    @objc dynamic var lastWatering =
//
//    @objc dynamic var wateringfrequency =
//    @objc dynamic var lastWatering =
    
    func setupCalendar () {
        let calendar = Calendar.current
        dayNeedToWatering = calendar.date(byAdding: .day, value: wateringfrequency, to: lastWatering)
    }                                                                                                                                
    
    convenience init(name: String, photoData: Data? = UIImage(named:"DefaultPlant")!.pngData(), wateringfrequency: Int?, lastWatering: Date? = nil  ) {
        self.init()
        self.name = name
        
        if photoData != nil {
            self.photoData = photoData!
        }
        
        if wateringfrequency != 0 {
            self.wateringfrequency = wateringfrequency!
        }
        
        if lastWatering != nil {
            self.lastWatering = lastWatering!
        }
//
    }
    
    
    
}
//UIImage(named:"DefaultPlant")!.pngData()
//UIImage(named:"DefaultPlant")!.jpegData(compressionQuality: 1.0)


//let realm = try! Realm() // Доступ к хранилищу
//
//var plants: Results<PlantList>! // контейнер со свойствами объекта PlantList


